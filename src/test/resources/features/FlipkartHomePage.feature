@FlipkartHomePage
Feature: Validating with Multiple Products

  Background: Open Browser
    Given Launch Browser "Chrome"
  @MultipleData
  Scenario Outline: Verify with multiple product for the same device
    Given Launch URL "https://www.flipkart.com/"
    When Search for Product "<Product>"
    And Click on Enter
    And Click on Popularity
#    And Get First Product Title
    And Select the First Product "<Product>"
    And Switch to Secondary Window <IndexofWindow>
    And Click on Buy Now Button

    Examples:
      | Product                              |IndexofWindow|
      | Iphone 15pro Mobile                   |1            |
      | Iphone 15pro Mobile Charger           |1            |
      | Iphone 15pro Mobile case              |1            |
      | Iphone 15pro Mobile case ScreenGuard  |1            |
