package com.pom.cucumber.locator;

public class HomePageLocator {

    //In HomePage
    //For Login
    protected String login_btn_xpath = "//span[text()='Login']";
    protected String emailORMobile_txt_Xpath = "//span[text()='Enter Email/Mobile number']//parent::label//preceding-sibling::input";
    protected String requestOTP_btn_xpath = "//button[text()='Request OTP']";

    //In Search Box
    protected String searchBox_xpath = "//input[@name='q']";
    //"//input[@title='Search for Products, Brands and More'and@name='q']";

    protected String sortBy_popularity_btn_xpath = "//div[text()='Popularity']";
    protected String firstProduct_xpath = "//div[@class='tUxRFH']/a[1]";
    //protected String get_TitleOfFirstProduct_xpath = "//div[@class='KzDlHZ'][1]";

    //In Secondary Window
    protected String buyNow_btn_xpath = "//button[text()='Buy Now']";

    // In Charger
    protected String firstCharger_Xpath = "//div[@class='slAVV4']/a[1]";
    protected String get_TitleOf_FirstCharger = "//div[@class='slAVV4']/a[1]//following-sibling::a[1]";


    //In mobile Case
    protected String firstMobileCase_Xpath = "//div[@class='slAVV4']/a[1]";
    protected String gettitle_of_firstMobileCase = "//div[@class='slAVV4']/a[1]//following-sibling::a[1]";


    //In ScreenGuard
    protected String firstScreenguard_Xpath = "//div[@class='slAVV4']/a[1]";
    protected String gettitle_of_firstScreenguard_xpath = "//div[@class='slAVV4']/a[1]//following-sibling::a[1]";
}
