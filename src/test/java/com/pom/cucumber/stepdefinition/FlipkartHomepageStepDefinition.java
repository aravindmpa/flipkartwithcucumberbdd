package com.pom.cucumber.stepdefinition;


import com.pom.cucumber.page.FlipkartHomepage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;

public class FlipkartHomepageStepDefinition {
    WebDriver driver;
    FlipkartHomepage fhm ;

    @Given("Launch Browser {string}")
    public void launch_browser(String browser_Name) {
        if (browser_Name.equalsIgnoreCase("edge")) {
           WebDriverManager.edgedriver().setup();
            driver = new EdgeDriver();
        } else if (browser_Name.equalsIgnoreCase("chrome")) {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
        } else if (browser_Name.equalsIgnoreCase("firefox")) {
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();
        } else if (browser_Name.equalsIgnoreCase("Safari")) {
            WebDriverManager.safaridriver().setup();
            driver = new SafariDriver();
        }
        fhm=new FlipkartHomepage(driver);

    }
    @Given("Launch URL {string}")
    public void launch_url(String url) {
        driver.get(url);
    }
    @When("Search for Product {string}")
    public void search_for_product(String Product) throws InterruptedException {
    fhm.enterValues_in_searchBox(Product);
    }
    @When("Click on Enter")
    public void click_on_enter() {
        fhm.keys_Enter();
    }
//    @When("Get First Product Title")
//    public void get_first_product_title() {
//       fhm.getProductTitle();
//
//    }
    @When("Click on Popularity")
    public void click_on_popularity() {
      fhm.sortBy_Popularity();
    }
    @When("Select the First Product {string}")
    public void select_the_first_product(String Product) throws InterruptedException {
        fhm.selectTheFirstProduct(Product);
    }
    @When("Switch to Secondary Window {int}")
    public void switch_to_secondary_window(int switchToIndex) {
    fhm.switchtoWindow(switchToIndex);
    }
    @When("Click on Buy Now Button")
    public void click_on_buy_now_button() throws InterruptedException {
        fhm.click_onBuyNow_btn();
    }


}
