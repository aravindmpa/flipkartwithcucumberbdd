package com.pom.cucumber.page;

import com.pom.cucumber.locator.HomePageLocator;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

public class FlipkartHomepage extends HomePageLocator {

    WebDriver driver;
    String productDetails;
    public FlipkartHomepage(WebDriver driver){
        this.driver=driver;

    }
//    public void selectBrowser(String browser_Name){
//        if (browser_Name.equalsIgnoreCase("edge")) {
//            WebDriverManager.edgedriver().setup();
//            driver = new EdgeDriver();
//        } else if (browser_Name.equalsIgnoreCase("chrome")) {
//            WebDriverManager.chromedriver().setup();
//            driver = new ChromeDriver();
//        } else if (browser_Name.equalsIgnoreCase("firefox")) {
//            WebDriverManager.firefoxdriver().setup();
//            driver = new FirefoxDriver();
//        } else if (browser_Name.equalsIgnoreCase("Safari")) {
//            WebDriverManager.safaridriver().setup();
//            driver = new SafariDriver();
//        }
//        FlipkartHomepage fhm = new FlipkartHomepage(driver);
  //  }
    public void enterValues_in_searchBox(String productDetails){
      driver.findElement(By.xpath(searchBox_xpath)).sendKeys(productDetails);
    }
    public void keys_Enter(){
        WebElement enter = driver.findElement(By.xpath(searchBox_xpath));
        enter.sendKeys(Keys.ENTER);
    }
    public void sortBy_Popularity(){
    driver.findElement(By.xpath(sortBy_popularity_btn_xpath)).click();
    }
    public void  selectTheFirstProduct(String productDetails) throws InterruptedException {

        Actions ma = new Actions(driver);
        if(productDetails.contains("Charger")) {
            System.out.println("Charger Exist");
            Thread.sleep(3000);
            WebElement firstProductCharger = driver.findElement(By.xpath(firstCharger_Xpath));
            ma.click(firstProductCharger).build().perform();
        } else if (productDetails.contains("case")) {
            Thread.sleep(3000);
            WebElement firstMobileCase = driver.findElement(By.xpath(firstMobileCase_Xpath));
            ma.click(firstMobileCase).build().perform();
        } else if (productDetails.contains("ScreenGuard")) {
            Thread.sleep(3000);
            WebElement firstScreenguard = driver.findElement(By.xpath(firstScreenguard_Xpath));
            ma.click(firstScreenguard).build().perform();
        }else{
            Thread.sleep(3000);
            WebElement firstProduct = driver.findElement(By.xpath(firstProduct_xpath));
            ma.click(firstProduct).build().perform();
        }
        Thread.sleep(2000);
    }
//    public String getProductTitle(){
//    WebElement ele_title =driver.findElement(By.xpath(get_TitleOfFirstProduct_xpath));
//    String title =ele_title.getText();
//        return title;
//    }
    public void switchtoWindow(int switchToIndex){
        Set<String> windowHandles = driver.getWindowHandles();
        ArrayList<String> allWindowIDs = new ArrayList<String>();
        Iterator<String> itr = windowHandles.iterator();
        System.out.println(windowHandles.size());
        for (int i = 0; i < windowHandles.size(); i++) {
            while (itr.hasNext()) {
                String windowID = (String) itr.next();
                System.out.println(windowID);
                allWindowIDs.add(windowID);
            }
        }
        //System.out.println(allWindowIDs.lastIndexOf(allWindowIDs));
        String childWindow = allWindowIDs.get(switchToIndex);
        // System.out.println(childWindow);
        driver.switchTo().window(childWindow);

    }
public void click_onBuyNow_btn() throws InterruptedException {
    WebElement buyNow_btn = driver.findElement(By.xpath(buyNow_btn_xpath));
    buyNow_btn.click();
    Thread.sleep(2000);
}

}
