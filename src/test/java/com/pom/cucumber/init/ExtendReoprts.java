//package com.pom.cucumber.init;
//
//import com.aventstack.extentreports.ExtentReports;
//import com.aventstack.extentreports.ExtentTest;
//import com.aventstack.extentreports.reporter.ExtentSparkReporter;
//import com.aventstack.extentreports.reporter.configuration.Theme;
//import org.openqa.selenium.OutputType;
//import org.openqa.selenium.TakesScreenshot;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.io.FileHandler;
//import org.testng.annotations.BeforeTest;
//import org.testng.annotations.Test;
//
//import java.io.File;
//
//public class ExtendReoprts {
//    ExtentReports extent = new ExtentReports();
//    ExtentSparkReporter spark = new ExtentSparkReporter("src\\test\\resources\\reports\\extendreport.html");
//
//    WebDriver driver;
//
//    @BeforeTest
//    public void setup() {
//        spark.config().setTheme(Theme.STANDARD);
//        spark.config().setDocumentTitle("Testing Extend Report");
//        extent.attachReporter(spark);
//    }
//
//    @Test
//    public void testValidation(String testcase,String passcondition) {
//        ExtentTest test = extent.createTest("Validating Flipkart Search").assignAuthor("Aravind Arumugam")
//                .assignCategory("Smoke Test").assignDevice("Windows");
//
//        test.info(testcase);
//        if (passcondition) {
//            test.pass("Google Page Title is Matched");
//        } else {
//            String screenshot_path = takeScreenshot();
//            test.fail("Google Page Title is not matched").addScreenCaptureFromPath(screenshot_path);
//
//        }
//    }
//
//    public String takeScreenshot() {
//        File dest = null;
//        try {
//            File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
//            dest = new File(System.getProperty("user.dir") + "src\\test\\resources\\failedtestcaseimage\\err_"
//                    + System.currentTimeMillis() + ".png");
//            FileHandler.copy(src, dest);
//
//        } catch (Exception e) {
//
//        }
//        return dest.getAbsolutePath();
//    }
//
//}